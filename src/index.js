import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import UserInput from './UserInput';
import UserOutput from './UserOutput';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(<UserInput />, document.getElementById('root'));
// ReactDOM.render(<UserOutput />, document.getElementById('root'));
registerServiceWorker();
