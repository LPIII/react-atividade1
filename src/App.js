import React, { Component } from 'react';
import './App.css';
import Pessoa from './Pessoa/Pessoa';
import UserInput from './UserInput';
import UserOutput from './UserOutput';

class App extends Component {

	state = {
		output: [
			{
				p1: 'Alisson Galiza',
				p2: 'Rhenan Castelo White'
			},

			{
				p1: 'Luiz Henrique',
				p2: 'Joao'
			},

			{
				p1: 'Aline',
				p2: 'Raoni'
			}
		]
	}

	/** Atualiza o p1 de todos os outputs*/
	mudaNome = () => {
		this.setState(
			{
				output:
					[
						{
							p1: document.getElementById('input').value + ' output 1',
							p2: this.state.output[0].p2
						},
						{
							p1: document.getElementById('input').value + ' output 2',
							p2: this.state.output[1].p2
						},
						{
							p1: document.getElementById('input').value + ' output 3',
							p2: this.state.output[2].p2
						}
					]
			}
		)
	}

	/** Atualiza o p1 do output especifico */
	mudaNome1 = (posicao) => {

		// copia o output
		var newOutput = this.state.output;
		// atualiza apenas o campo que queremos
		newOutput[posicao].p1 = document.getElementById('input').value + ' output ' + posicao + 1;

		// Atualiza estado
		this.setState(
			{
				output: newOutput
			}
		)


	}

	

	render() {
		// console.log(this.mudaNome1(0));
		return (
			<div className="App">
				<UserInput />
				<UserOutput p1={this.state.output[0].p1}
					p2={this.state.output[0].p2}
					mudaNome={this.mudaNome}
					mudaNome1={this.mudaNome1}
					posicao={0} />

				<UserOutput p1={this.state.output[1].p1}
					p2={this.state.output[1].p2}
					mudaNome={this.mudaNome}
					mudaNome1={this.mudaNome1}
					posicao={1} />

				<UserOutput p1={this.state.output[2].p1}
					p2={this.state.output[2].p2}
					mudaNome={this.mudaNome}
					mudaNome1={this.mudaNome1}
					posicao={2} />

			</div>

		);
	}
}

export default App;
