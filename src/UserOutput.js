import React, {Component} from 'react';
import './UserOutput.css'


class UserOutput extends Component {
    
    posicao = undefined;
    // Construtor para o render
    UserOutput  = function  (props) {
        this.props = props;
        this.posicao = props.posicao;
        console.log(this.posicao);
    }

    render() {
        
        return (
            <div className="output">
                <p className="p1" id='p1'>
                    {this.props.p1}
                </p>
                <p className="p2" id='p2'>
                    {this.props.p2}
                </p>
                <button onClick={this.props.mudaNome}>
                    Mudar
                </button>
                <button onClick={() => this.props.mudaNome1(this.props.posicao)}>
                    Mudar unico
                </button>

            </div>
        )

    }
}

// Funciona. Não toque!!
// const UserOutput = function (props) {
//     return (
//         <div>
//             <p id='p1'>
//                 {props.p1}
//             </p>
//             <p id='p2'>
//                 {props.p2}
//             </p>
//         </div>
//     )
// }



export default UserOutput;