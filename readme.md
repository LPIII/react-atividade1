## Como intalar

```sh
sudo apt-get install -y curl
```

```sh
sudo curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash
```

```sh
sudo apt-get install -y nodejs
```

```sh
sudo npm install -g create-react-app
```


Criando repositório

```sh
sudo create-react-app aula01
cd aula01
```

Agora coloque as pastas src e public na pasta 'aula01' que voceê acabou de criar e rode o seguinte comando    
    
```sh
npm start
```

## atividades:
Crie DOIS novos componentes: UserInput e UserOutput

UserInput deve conter um elemento de entrada, UserOutput dois parágrafos

Use de vários componentes UserOutput no componente App (com quaisquer textos de parágrafo de sua escolha)

Passar pelo app um nome de usuário (de sua escolha) para UserOutput via props e exibia-as

Adicione state ao componente App (=> o nome de usuário) e passe o nome de usuário para o componente UserOutput

Adicione um método para manipular o estado (=> um método manipulador de eventos)

Passe a referência de método manipulador de eventos ao componente UserInput e faça um bind ao evento de alteração de entrada

Assegure-se de que a nova entrada inserida pelo usuário substitua o nome de usuário antigo passado para UserOutput

Adicione uma two-way-binding à sua entrada (em UserInput) para também exibir o nome de usuário inicial que está no App

Adicione um estilo de sua escolha aos seus componentes / elementos nos componentes - utilizando as duas opções apresentadas na sala de aula 